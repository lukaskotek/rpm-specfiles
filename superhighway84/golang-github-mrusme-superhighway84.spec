# Generated by go2rpm 1.6.0
%bcond_without check

# https://github.com/mrusme/superhighway84
%global goipath         github.com/mrusme/superhighway84
Version:                0.1.1

%gometa

%global common_description %{expand:
USENET-inspired, uncensorable, decentralized internet discussion system.}

%global golicenses      LICENSE
%global godocs          README.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        USENET-inspired, uncensorable, decentralized internet discussion system

# Upstream license specification: GPL-3.0-only
License:        GPLv3
URL:            %{gourl}
Source0:        %{gosource}

%description
%{common_description}

%gopkg

%prep
%goprep

%generate_buildrequires
%go_generate_buildrequires

%build
%gobuild -o %{gobuilddir}/bin/superhighway84 %{goipath}

%install
%gopkginstall
install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/

%if %{with check}
%check
%gocheck
%endif

%files
%license LICENSE
%doc README.md
%{_bindir}/*

%gopkgfiles

%changelog
* Tue Apr 12 2022 Elagost <adamj@mailbox.org> - 0.1.1-1
- Initial package
